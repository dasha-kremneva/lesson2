import java.time.LocalDateTime;

public class Main {

    public static void main(String[] args) {
        LocalDateTime date = LocalDateTime.now();
        Recipient recipient = new Recipient("Кремнева", "Дарья", "Павловна", 5000, "11111", "11111111");
        Sender sender = new Sender("Куликова", "Наталья", "Игоревна", 50000, "22222", "222222222");
        new TransferCard(4000, Currency.РУБЛЬ, recipient, sender).doTransfer();
        System.out.println(recipient.getAmountMoney());
        System.out.println(sender.getAmountMoney());
        new TransferAccount(2000, Currency.РУБЛЬ, recipient, sender).doTransfer();
        System.out.println(recipient.getAmountMoney());
        System.out.println(sender.getAmountMoney());

    }
}
