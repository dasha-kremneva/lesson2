import java.time.LocalDateTime;

enum Currency {ДОЛЛАР, РУБЛЬ, ТЕНГЕ, ФРАНК, КРОНА, ЕВРО}

public abstract class Transfer {

    protected Currency currency = Currency.РУБЛЬ;
    protected int numberTransfer = 1;
    protected double sumTransfer;
    protected Recipient recipient;
    protected Sender sender;
    private int transferID;

    public Transfer(double sumTransfer, Currency currency, Recipient recipient, Sender sender) {
        this.sumTransfer = sumTransfer;
        this.currency = currency;
        this.recipient = recipient;
        this.sender = sender;
        this.transferID = numberTransfer++;
    }

    public void doTransfer() {
        if (sender.isMoneyMoreThan(sumTransfer)) {
            //уменьшение счета отправителя
            sender.setAmountMoney(sender.getAmountMoney() - sumTransfer);
            //увеличениче счета получателя
            recipient.setAmountMoney(recipient.getAmountMoney() + sumTransfer);
            System.out.println(this);
        } else {
            System.out.println("На вашей карте недостаточно средств для совершения перевода");
        }
    }

    @Override
    public String toString() {
        return ", Номер первода: " + numberTransfer +
                ", Дата: " + LocalDateTime.now() +
                ", Валюта: " + currency +
                ", Сумма: " + sumTransfer +
                ", Получатель: " + recipient.getFIO() +
                ", Отправитель: " + sender.getFIO()
                ;
    }


    public int getNumberTransfer() {
        return numberTransfer;
    }

    public double getSumTransfer() {
        return sumTransfer;
    }

    public void setSumTransfer(double sumTransfer) {
        this.sumTransfer = sumTransfer;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
