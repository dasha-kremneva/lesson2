public class Recipient extends Person {
    public Recipient(String surname, String name, String patronymic, double amountMoney, String numberCard, String numberAccount) {
        super(surname, name, patronymic, amountMoney, numberCard, numberAccount);
    }
}
