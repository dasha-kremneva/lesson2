
public class TransferCard extends Transfer {

    public TransferCard(double sumTransfer, Currency currency, Recipient recipient, Sender sender) {
        super(sumTransfer, currency, recipient, sender);
    }

    @Override
    public String toString() {
        return "ПЕРЕВОД С КАРТЫ НА КАРТУ: " +
                ", Номер карты получателя: " + recipient.getNumberCard() +
                ", Номер карты отправителя: " + sender.getNumberCard() +
                super.toString();
    }
}
