//отправитель
public class Sender extends Person {
    public Sender(String surname, String name, String patronymic, double amountMoney, String numberCard, String numberAccount) {
        super(surname, name, patronymic, amountMoney, numberCard, numberAccount);
    }

    public boolean isMoneyMoreThan(double sumTransfer) {
        return getAmountMoney() >= sumTransfer;
    }
}
