
public class TransferAccount extends Transfer {

    public TransferAccount(double sumTransfer, Currency currency, Recipient recipient, Sender sender) {
        super(sumTransfer, currency, recipient, sender);
    }

    @Override
    public String toString() {
        return "ПЕРЕВОД СО СЧЕТА НА СЧЕТ: " +
                ", Номер счета получателя: " + recipient.getNumberAccount() +
                ", Номер счета отправителя: " + sender.getNumberAccount() +
                super.toString();
    }
}
