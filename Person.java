public class Person {

    private String numberCard;
    private String numberAccount;
    private String surname;
    private String name;
    private String patronymic;
    private double amountMoney;

    public Person(String surname, String name, String patronymic, double amountMoney,String numberCard,String numberAccount){
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.numberCard = numberCard;
        this.numberAccount = numberAccount;
        this.amountMoney = amountMoney;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Person guest = (Person) obj;
        return  name == guest.name ||
                (name != null &&name.equals(guest.getName())) && (surname == guest.surname
                        || (surname != null && surname .equals(guest.getSurname())
                ));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((surname == null) ? 0 : surname.hashCode()); return result;
    }

    public String getFIO() {
        return surname + " " + name + " " + patronymic;
    }

    public String getUpperName() {
        return getFIO().toUpperCase();
    }

    public String getLowerName() {
        return getFIO().toLowerCase();
    }

    public double getAmountMoney() {
        return amountMoney;
    }

    public void setAmountMoney(double amountMoney) {
        this.amountMoney = amountMoney;
    }

    public String getNumberCard() {
        return numberCard;
    }

    public String getNumberAccount() {
        return numberAccount;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }
}
